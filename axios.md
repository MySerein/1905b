# axios二次封装
1.前后端交互方式
&emsp;原生ajax
&emsp;&emsp;jQ ajax
&emsp;axios(基于promise封装的ajax),在浏览器和node端都可使用
fetch(es6内置类,天生就是由promise构成)

1.设置请求拦截器,把config拦截设置后返回,发生在请求之前,一般用于身份验证等。
````
  axios.interceptors.request.use(function (config) {
        let token=localStorage.getItem("token")
        //设置token
        token ? config.headers["Authorization"] = token:null
    return config;
  }, function (error) {
    return Promise.reject(error);
  });
````
2.设置响应拦截器
````
    axios.interceptors.response.use(function (response) {
        //请求成功回来response对象属性
        //data,status/statusText,request(xhr对象),headers,config
         return response;
    }, function (error) {
        //网络问题,或者弱网环境
        if(error.code==='ECONNABORTED'){
             alert('网页加载超时，用户网速较低，请重新加载或请求')
           }
        //请求失败
        let status=error.response.status;
            switch(status){
                case 401:   //访问权限
                break;
                case:403    //服务端拒绝执行(可能没有登录)
                break;
                case 404:   //页面找不到
                break;
                case 500:   //服务端错误
                break;
                case 503:   //服务端繁忙
                break；
     }
       return Promise.reject(error);
  });
````

总结
axios的二次封装,就是封装一些公共的内容,公共的options,请求拦截器,响应拦截器。

调用use都传入两个函数,一个成功调用,一个失败调用,请求拦截器,一般设置token等公共的请求头,响应拦截器会在成功时设置只返回响应主体,在请求失败后,判断是网络问题还是客户端或服务端问题(status以4/5开头的),根据不同的响应返回不同的结果。
