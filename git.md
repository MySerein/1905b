# git总结
1.Git 是一个开源的分布式版本控制系统。

2.工作区（working） - 当你 git clone 一个项目到本地，相当于在本地克隆了项目的一个副本。工作区是对项目的某个版本独立提取出来的内容。这些从 Git 仓库的压缩数据库中提取出来的文件，放在磁盘上供你使用或修改。

3.暂存区（staging）- 暂存区是一个文件，保存了下次将提交的文件列表信息，一般在 Git 仓库目录中。有时候也被称作 `‘索引’'，不过一般说法还是叫暂存区。

4.本地仓库（local） - 提交更新，找到暂存区域的文件，将快照永久性存储到 Git 本地仓库。

5.远程仓库（remote） - 以上几个工作区都是在本地。为了让别人可以看到你的修改，你需要将你的更新推送到远程仓库。同理，如果你想同步别人的修改，你需要从远程仓库拉取更新。

6.分支（Branch）
分支是为了将修改记录的整个流程分开存储，让分开的分支不受其它分支的影响，所以在同一个数据库里可以同时进行多个不同的修改

7.主分支（main）前面提到过 main 是 Git 为我们自动创建的第一个分支，也叫主分支，其它分支开发完成后都要合并到 master

8.创建仓库
克隆一个已创建的仓库：
````
# 通过 SSH
 git clone ssh://user@domain.com/repo.git

#通过 HTTP
 git clone http://domain.com/user/repo.git
````
9.初始化git：
````
 git init
````

10.添加修改 添加修改到暂存区：
````
# 把指定文件添加到暂存区
 git add xxx

# 把当前所有修改添加到暂存区
 git add .

# 把所有修改添加到暂存区
 git add -A
````

11.提交修改到本地仓库：
````
# 提交并附带更新了什么
 git commit -m '信息'
````

12.储藏
有时，我们需要在同一个项目的不同分支上工作。当需要切换分支时，偏偏本地的工作还没有完成，此时，提交修改显得不严谨，但是不提交代码又无法切换分支。这时，你可以使用 git stash 将本地的修改内容暂存
````
# 1. 将修改暂存
 git stash

# 2 恢复之前修改
 git stash pop

````
13.拉取远程分支

````
# 1. 拉去单个远程分支
 git fetch <remote>

# 2. 拉取全部远程分支
 git fetch
````
13.查看信息
查看当前分支状态:
````
 git status
````

14.显示提交历史：
````
# 显示所有的提交记录
 git log
````

15.分支
增删查分支：
````

# 列出所有的分支
 git branch

# 列出所有的远端分支
 git branch -r

# 基于当前分支创建新分支
 git branch <new-branch>

# 删除本地分支
 git branch -d <branch>
````

16.切换分支:
````
# 切换分支
 git checkout <branch>

# 创建并切换到新分支
 git checkout -b <branch>

 # 创建并和远程分支产生关联
 git checkout -b <branch> origin/<branch>
````

17.合并:
````
# 将分支合并到当前分支
 git merge <branch>
````

18.拉取:
````
# 从远程获取最新版本并merge到本地
 git pull
````

18.分支命名
1、master 分支

master 为主分支，也是用于部署生产环境的分支，确保master分支稳定性， master 分支一般由develop以及hotfix分支合并，任何时间都不能直接修改代码

2、develop 分支

develop 为开发分支，始终保持最新完成以及bug修复后的代码，一般开发的新功能时，feature分支都是基于develop分支下创建的。

feature 分支
开发新功能时，以develop为基础创建feature分支。 分支命名: feature/ 开头的为特性分支， 命名规则: feature/user_module、 feature/cart_module

release分支
release 为预上线分支，发布提测阶段，会release分支代码为基准提测。当有一组feature开发完成，首先会合并到develop分支，进入提测时会创建release分支。如果测试过程中若存在bug需要修复，则直接由开发者在release分支修复并提交。当测试完成之后，合并release分支到master和develop分支，此时master为最新代码，用作上线。

hotfix 分支
分支命名: hotfix/ 开头的为修复分支，它的命名规则与feature分支类似。线上出现紧急问题时，需要及时修复，以master分支为基线，创建hotfix分支，修复完成后，需要合并到master分支和develop分支
