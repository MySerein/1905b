# 项目基础建设、目录结构规范
## 一. 项目基础建设
1.团队约定

&emsp;husky

&emsp;第二封装git hooks；

&emsp;用来在git不同阶段执行不同任务的


````
 下包
 yarn add husky
````

&emsp;执行生成.husky文件夹

````
  husky install
````

&emsp;不想全局安装就通过scripts执行指令
 ````
 "scripts":{
   "husky":"husky install"
 }
 ````


&emsp;规范commit message信息 , 添加钩子（hook） 动作
````
npx husky add .husky/commit-msg "npx --no-install commitlint --edit"
````
&emsp;测试 
````
  git commit -m ""
````
&emsp;

&emsp;eslint

&emsp;下载 lint-staged

&emsp;在 scripts 字段添加lint运行 lint-staged指令

&emsp;监听文件变化

&emsp;添加git hooks pre-commit   git add
````
  npx husky add .husky/pre-commit "npm run lint"
````
执行该命令后，会看到.husky/目录下新增了一个名为pre-commit的shell脚本。
这样，在之后执行git commit命令时会先触发pre-commit这个脚本。

1.husky 生成钩子 npx husky add .husky/钩子名称 钩子执行的指令
2.commitlint 配置message提交信息
3.lint-staged 检测文件变化执行eslint修复

## 二. 开发基础建设
&emsp;1. 配置别名 
````
避免出现 ../../../文件 这样形式的路径
resolve:{
  alias:{
    '名称':"路径"
  }
}
````
&emsp;2. api拆分

&emsp;统一管理接口地址
&emsp;接口方法复用

&emsp;3.接口请求跨域问题

&emsp;在package.json中写入proxy(反响代理,服务器去请求数据) 解决跨域问题

# 三. 目录结构规范
&emsp;1.目录文件

&emsp;assets：放置原始资源文件。

&emsp;components：存放全局组件。

&emsp;store: 仓库文件。

&emsp;pages：页面文件夹。

&emsp;router：路由文件夹。

&emsp;utils：存放一些常用函数的封装。

&emsp;2.命名规范
````
kebab-case：短横线单词连接命名法，全部小写。
camelCase：驼峰命名法（俗称小驼峰式命名法），第一个单字以 小写字母开始；第二个单字的首字母大写。
PascalCase：帕斯卡命名法（（俗称大驼峰式命名法），每一个单字的首字母都采用 大写字母。
````
